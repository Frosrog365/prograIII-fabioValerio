﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2
{
    class Program
    {
        static void Main(string[] args)
        {
            Logica log = new Logica();
            Console.WriteLine("Escriba el primer número");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Escriba el segundo número");
            int num2 = int.Parse(Console.ReadLine());
            Console.WriteLine(log.MCD(num1, num2));
            Console.WriteLine("Escriba la cantidad de meses");
            int meses = int.Parse(Console.ReadLine());
            Console.WriteLine("La cantidad de conejos en {0} meses es de {1}", meses, log.CantConejos(meses));
            Console.WriteLine("Escriba la cantidad de conejos");
            int conejos = int.Parse(Console.ReadLine());
            Console.WriteLine("Va a tener {0} conejos en {1} meses", conejos, log.CantMeses(conejos));
            Console.WriteLine("Digite el numero: ");
            int numero = int.Parse(Console.ReadLine());
            Console.WriteLine(log.EsPrimo(numero));
            Console.WriteLine("Total de segundos en sacar los 10k: {0:0}", log.DiezMilPrimos());

            Console.WriteLine("8 es perfecto: {0}", log.EsPerfecto(8));
            Console.WriteLine("6 es perfecto: {0}", log.EsPerfecto(6));
            Console.WriteLine("2 es perfecto: {0}", log.EsPerfecto(2));
            Console.WriteLine("28 es perfecto: {0}", log.EsPerfecto(28));


            Console.ReadKey();
        }
    }

}
