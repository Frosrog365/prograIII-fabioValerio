﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2
{
    class Logica
    {
        /// <summary>
        /// Sacar el máximo común divisor de dos números
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="n2"></param>
        /// <returns></returns>
        internal int MCD(int n1, int n2)
        {
            int mcd = 1;
            int div = 2;
            while (n1 >= div && n2 >= div)
            {
                if (n1 % div == 0 && n2 % div == 0)
                {
                    mcd *= div;
                    n1 /= div;
                    n2 /= div;
                }
                else
                {
                    div++;
                }
            }
            return mcd;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="meses"></param>
        /// <returns></returns>
        internal int CantConejos(int meses)
        {
            int fertiles = 1;
            int crias = 0;
            for (int i = 0; i < meses; i++)
            {
                int temp = fertiles;
                fertiles += crias;
                crias = temp;
            }
            return (crias + fertiles) * 2;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conejos"></param>
        /// <returns></returns>
        internal int CantMeses(int conejos)
        {
            int meses = 0;
            int cantConejos = 0;
            int fertiles = 2;
            int crias = 0;
            while(conejos> cantConejos)
            {
                int temp = fertiles;
                fertiles = fertiles + crias;
                crias = temp;
                meses++;
                cantConejos = (fertiles + crias * 2);
            }
            return meses;
        }
        internal bool EsPrimo(int numero)
        {
            int div = 0;

            for (int i = 1; i < numero; i++)
            {
                if(numero% i == 0)
                {
                    div++;
                }
                if(div == 2)
                {
                    break;
                }
            }
            return div == 2; 

        }
        internal double DiezMilPrimos()
        {
            DateTime d1 = DateTime.Now;
            int con = 0;
            int num = 0;
            while (con < 10000)
            {
                if (EsPrimo(num))
                {
                    con++;
                }
                num++;
            }
            return DateTime.Now.Subtract(d1).TotalSeconds;
        }
        internal bool EsPerfecto(int num)
        {
            int sumDiv = 0;
            for (int i = 1; i < num; i++)
            {
                if(num % i == 0)
                {
                    sumDiv += 1;
                }
            }
            return sumDiv == num;
        }
    }
}
