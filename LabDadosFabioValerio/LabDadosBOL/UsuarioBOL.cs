﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabDadosDAL;
using LabDadosEN;

namespace LabDadosBOL
{

    public class UsuarioBOL
    {
        private UsuarioDAL dal;
        public UsuarioBOL()
        {
            dal = new UsuarioDAL();
        }
        public void IniciarSesion()
        {
            throw new NotImplementedException();
        }

        public Usuario IniciarSesion(Usuario usuario)
        {
            if (String.IsNullOrEmpty(usuario.NombreUsuario))
            {
                return null;
            }

            if (String.IsNullOrEmpty(usuario.Pass))
            {
                return null;
            }


            return dal.Verificar(usuario);
        }

        public void guardarJugada(Jugada jugada)
        {
            dal.GuardarJugada(jugada);
        }

        public bool ValidadDados(string v, List<int> dado1, List<int> dado2)
        {
            int suma = dado1.Last() + dado2.Last();
            if (Convert.ToInt32(v) == suma)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
