﻿using LabDadosEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabDadosBOL
{
    public class DadosBOL
    {
        public bool JugadasHechas(Usuario usuario)
        {
            if (usuario.Jugadas.cantidad < 2)
            {
                usuario.Jugadas.cantidad++;
            }
            else if (usuario.Jugadas.cantidad == 2 && usuario.Jugadas.hoy.Equals(System.DateTime.Now.ToShortDateString()))
            {
                Console.WriteLine(usuario.Jugadas.hoy);
                Console.WriteLine(System.DateTime.Now.ToShortDateString());
                return true;
            }
            else if (usuario.Jugadas.cantidad == 2 && !usuario.Jugadas.hoy.Equals(System.DateTime.Now.ToShortDateString()))
            {
                usuario.Jugadas.cantidad = 0;
            }

            return false;
        }


        public List<int> SacarDado1(Random rnd)
        {
            List<int> dado1 = new List<int>();

            for (int i = 0; i < 6; i++)
            {
                dado1.Add(rnd.Next(1, 7));
            }
            return dado1;
        }

        public List<int> SacarDado2()
        {
            List<int> dado2 = new List<int>();
            Random rnd = new Random();

            for (int i = 0; i < 6; i++)
            {
                dado2.Add(rnd.Next(1, 7));
            }
            return dado2;
        }

    }
}
