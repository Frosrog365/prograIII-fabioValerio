﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LabDadosBOL;
using LabDadosEN;

namespace LabDadosFabioValerio
{
    public partial class Juego : Form
    {
        DadosBOL dados;
        List<int> dado1=new List<int>();
        List<int> dado2=new List<int>();
        int inicio1 = 0;
        int inicio2 = 0;
        Usuario user;
        UsuarioBOL bol;

        public Juego()
        {
            InitializeComponent();
        }

        public Juego(Usuario iniciado)
        {
            InitializeComponent();
            user = iniciado;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dados.JugadasHechas(user))
            {
                button1.Enabled = false;
                bol.guardarJugada(user.Jugadas);

            }
            dados = new DadosBOL();
            Random rnd = new Random();
            dado1 = dados.SacarDado1(rnd);
            dado2 = dados.SacarDado1(rnd);

            foreach (int item in dado1)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("\n");
            foreach (int item in dado2)
            {
                Console.WriteLine(item);
            }

            inicio1 = 0;
            
            timer1.Start();
            if(bol.ValidadDados(textBox1.Text.Trim(), dado1, dado2))
            {
                MessageBox.Show("Pegó 2 puntos");
            }
            
            
        }

        private void Juego_Load(object sender, EventArgs e)
        {
            dados = new DadosBOL();
            bol = new UsuarioBOL();
            user.Jugadas.hoy = DateTime.Now.ToShortDateString();
            if (dados.JugadasHechas(user))
            {
                button1.Enabled = false;
                bol.guardarJugada(user.Jugadas);

            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            if (inicio1 < dado1.Count()-1)
            {
                inicio1++;
            }else if(inicio1 == dado1.Count)
            {
                timer1.Stop();
                
            }

            switch (dado1.ElementAt(inicio1))
            {
                case 1:
                    pbxDado1.Image = global::LabDadosFabioValerio.Properties.Resources._1;
                    break;
                case 2:
                    pbxDado1.Image = global::LabDadosFabioValerio.Properties.Resources._2;
                    break;
                case 3:
                    pbxDado1.Image = global::LabDadosFabioValerio.Properties.Resources._3;
                    break;
                case 4:
                    pbxDado1.Image = global::LabDadosFabioValerio.Properties.Resources._4;
                    break;
                case 5:
                    pbxDado1.Image = global::LabDadosFabioValerio.Properties.Resources._5;
                    break;
                case 6:
                    pbxDado1.Image = global::LabDadosFabioValerio.Properties.Resources._6;
                    break;
            }


            switch (dado2.ElementAt(inicio1))
            {
                case 1:
                    pbxDado2.Image = global::LabDadosFabioValerio.Properties.Resources._1;
                    break;
                case 2:
                    pbxDado2.Image = global::LabDadosFabioValerio.Properties.Resources._2;
                    break;
                case 3:
                    pbxDado2.Image = global::LabDadosFabioValerio.Properties.Resources._3;
                    break;
                case 4:
                    pbxDado2.Image = global::LabDadosFabioValerio.Properties.Resources._4;
                    break;
                case 5:
                    pbxDado2.Image = global::LabDadosFabioValerio.Properties.Resources._5;
                    break;
                case 6:
                    pbxDado2.Image = global::LabDadosFabioValerio.Properties.Resources._6;
                    break;
            }
        
    }

        private void timer2_Tick(object sender, EventArgs e)
        {
            
        }
    }
}
