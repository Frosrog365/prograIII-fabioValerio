﻿using LabDadosBOL;
using LabDadosEN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabDadosFabioValerio
{
    public partial class Form1 : Form
    {
        UsuarioBOL bol;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario
            {
                NombreUsuario = txtUsuario.Text.Trim(),
                Pass = txtPass.Text.Trim()
            };
            usuario = bol.IniciarSesion(usuario);

            if (usuario != null)
            {
                Juego frm = new Juego(usuario);
                LimpiarDatos();
                frm.Show(this);
                Hide();
            }
            else
            {
                MessageBox.Show("Usuario/Contraseña inválidos");
            }
        }
        private void LimpiarDatos()
        {
            txtUsuario.Text = "";
            txtPass.Clear();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            bol = new UsuarioBOL();
        }
    }
}
