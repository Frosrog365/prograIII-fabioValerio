﻿namespace LabDadosFabioValerio
{
    partial class Juego
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbxDado2 = new System.Windows.Forms.PictureBox();
            this.pbxDado1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pbxDado2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDado1)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxDado2
            // 
            this.pbxDado2.Image = global::LabDadosFabioValerio.Properties.Resources._1;
            this.pbxDado2.Location = new System.Drawing.Point(135, 12);
            this.pbxDado2.Name = "pbxDado2";
            this.pbxDado2.Size = new System.Drawing.Size(105, 104);
            this.pbxDado2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxDado2.TabIndex = 1;
            this.pbxDado2.TabStop = false;
            // 
            // pbxDado1
            // 
            this.pbxDado1.Image = global::LabDadosFabioValerio.Properties.Resources._1;
            this.pbxDado1.Location = new System.Drawing.Point(12, 12);
            this.pbxDado1.Name = "pbxDado1";
            this.pbxDado1.Size = new System.Drawing.Size(102, 104);
            this.pbxDado1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxDado1.TabIndex = 0;
            this.pbxDado1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(105, 140);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Digite un número";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(63, 166);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 41);
            this.button1.TabIndex = 4;
            this.button1.Text = "GO!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 300;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Juego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(256, 261);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pbxDado2);
            this.Controls.Add(this.pbxDado1);
            this.Name = "Juego";
            this.Text = "Juego";
            this.Load += new System.EventHandler(this.Juego_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxDado2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDado1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxDado1;
        private System.Windows.Forms.PictureBox pbxDado2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
    }
}