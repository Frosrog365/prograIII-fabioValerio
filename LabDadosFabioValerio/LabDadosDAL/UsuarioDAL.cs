﻿using LabDadosEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace LabDadosDAL
{
    public class UsuarioDAL
    {
        public Usuario Verificar(Usuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();

                string sql = @"select id, usuario, pass, id_jug from usuario where (usuario = @usuario)
                                and pass = @pass";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usuario", usuario.NombreUsuario);
                cmd.Parameters.AddWithValue("@Pass", usuario.Pass);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarUsuario(reader);
                }
            }
            return null;
        }

        public void GuardarJugada(Jugada jugada)
        {
            {
                using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
                {

                    con.Open();
                    string sql = @"INSERT INTO jugada(hoy,cantidad) VALUES (@hoy, @cant) returning id";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@hoy", jugada.hoy);
                    cmd.Parameters.AddWithValue("@cant", jugada.cantidad);
                    jugada.Id = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
        }
        public Jugada cargarJugada(NpgsqlDataReader reader, int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();

                string sql = @"select id, hoy, cantidad from jugada where (id = @usuario_id)";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usuario_id", id);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    Jugada nueva = new Jugada
                    {
                        Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0,
                        hoy = reader["hoy"].ToString(),
                        cantidad = Convert.ToInt32(reader["cantidad"])
                    };
                    return nueva;
                }
                return null;
            }
        }

        private Usuario CargarUsuario(NpgsqlDataReader reader)
        {
            Usuario usu = new Usuario
            {
                Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0,
                NombreUsuario = reader["usuario"].ToString(),
                Pass = reader["pass"].ToString(),
                Jugadas = cargarJugada(reader, Convert.ToInt32(reader["id_jug"]))

            };

            return usu;
        }
    }
}
