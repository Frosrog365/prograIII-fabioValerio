﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos_Parte1
{
    class Rectangulo
    {
        public double Largo { get; set; }
        public double Ancho { get; set; }

        public double calcularArea()
        {
            return Largo * Ancho;
        }

    }
}
