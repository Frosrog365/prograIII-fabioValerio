﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos_Parte1
{
    class Fecha
    {
        int Mes;
        int Dia;
        int Anno;
        String fechaEnPalabras;

        public Fecha(int mes, int dia, int anno)
        {
            if (ValidarFecha())
            {
                Mes = mes;
                Dia = dia;
                Anno = anno;
            }
            else
            {
                Mes = 1;
                Dia = 1;
                Anno = 2018;
            }
        }

        public string MesPalabra()
        {
            switch (Mes)
            {
                case 1:
                    return fechaEnPalabras = Dia + " de Enero del " + Anno;
                    
                case 2:
                    return fechaEnPalabras = Dia + " de Febrero del " + Anno;
                    
                case 3:
                    return fechaEnPalabras = Dia + " de Marzo del " + Anno;
                    
                case 4:
                    return fechaEnPalabras = Dia + " de Abril del " + Anno;
                    
                case 5:
                    return fechaEnPalabras = Dia + " de Mayo del " + Anno;
                    
                case 6:
                    return fechaEnPalabras = Dia + " de Junio del " + Anno;
                    
                case 7:
                    return fechaEnPalabras = Dia + " de Julio del " + Anno;
                   
                case 8:
                    return fechaEnPalabras = Dia + " de Agosto del " + Anno;
                    
                case 9:
                    return fechaEnPalabras = Dia + " de Septiembre del " + Anno;
                    
                case 10:
                    return fechaEnPalabras = Dia + " de Octubre del " + Anno;
                  
                case 11:
                    return fechaEnPalabras = Dia + " de Noviembre del " + Anno;
                
                case 12:
                    return fechaEnPalabras = Dia + " de Diciembre del " + Anno;
                   
                default:
                    return "";
                    
            }
        }
   
        public bool ValidarFecha()
        {
            if (Mes < 0 || Mes > 12)
            {
                return false;
            }
            else if (Dia < 0)
            {
                return false;
            }
            else if (Dia > 28 && Mes != 2)
            {
                return false;
            }
            else if (Dia > 31)
            {
                return false;
            }
            else if (Dia == 31 && (Mes == 4 || Mes == 6 || Mes == 9 || Mes == 11))
            {
                return false;
            }else if(!EsBisiesto() && Mes == 2 && Dia == 29)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool EsBisiesto()
        {
            if (Anno % 4 == 0 && (Anno % 100 != 0 || Anno % 400 == 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
