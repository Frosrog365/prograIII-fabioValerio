﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos_Parte1
{
    class Circunferencia
    {

        public float area()
        {
            return Convert.ToSingle(Math.PI * Math.Pow(Radio, 2));
        }
        public float perimetro()
        {
            return Convert.ToSingle(2 * Math.PI * Radio);
        }
        public double Radio { get; set; }

    }
}
