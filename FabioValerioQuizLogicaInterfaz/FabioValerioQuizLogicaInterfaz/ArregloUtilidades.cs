﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FabioValerioQuizLogicaInterfaz
{
    class ArregloUtilidades
    {
        internal String ArregloToStr(Array a)
        {
            string resultado = "[";
            for (int i = 0; i < a.Length; i++)
            {
                resultado += string.Format("{0},", a.GetValue(i));
            }
            resultado = resultado.Substring(0, resultado.Length - 1);
            resultado += "]";
            return resultado;
        }
        internal int[] ArreglosRandom(String a)
        {
            Random rnd = new Random();
            int[] numeros = new int[int.Parse(a)];
            for (int i = 0; i < numeros.Length; i++)
            {
                numeros[i] = rnd.Next(1, 100);
            }
            return numeros;
        }
        internal string OrdenarAscendente(String numero)
        {
            Random rnd = new Random();
            int num = int.Parse(numero);
            string arregloStrOrd = "";
            string arregloStr = "";
            int[] arreglo = new int[num];
            for (int i = 0; i < arreglo.Length; i++)
            {
                arreglo[i] = rnd.Next(0, 100);
            }
            for (int i = 0; i < arreglo.Length; i++)
            {
                arregloStr += string.Format("{0},", arreglo[i]);

            }
            Array.Sort(arreglo);
            for (int i = 0; i < arreglo.Length; i++)
            {
                arregloStrOrd += string.Format("{0},", arreglo[i]);

            }


            return string.Format("El arreglo fue {0}\n" +
                "Y ordenado sería {1}", arregloStr, arregloStrOrd);
        }
        internal string rango(string a)
        {
            int num = 0;
            if (int.TryParse(a, out num))
            {
                Random rnd = new Random();
                int[] numeros = new int[int.Parse(a)];
                for (int i = 0; i < numeros.Length; i++)
                {
                    numeros[i] = rnd.Next(1, 100);
                }
                int menor = numeros.Min();
                int mayor = numeros.Max();
                string resultado = ArregloToStr(numeros);
                string resultadoFinal = String.Format("El arreglo es {0} y el rango es {1}", resultado, mayor - menor);
                return resultadoFinal;
            }
            else
            {
                return "Dato inválido, intente de nuevo";
            }
        }

        internal string promedio(string a)
        {
            int num = 0;
            if (!int.TryParse(a, out num))
            {
                return "Digite un número";
            }
            else
            {
                int[] numeros = ArreglosRandom(a);
                int suma = 0;
                for (int i = 0; i < numeros.Length; i++)
                {
                    suma += numeros[i];
                }
                return string.Format("El arreglo es {0} y el promedio es {1}", ArregloToStr(numeros), suma / num);

            }
        }
        internal string TodoJunto(string a)
        {
            int num = 0;
            if (!int.TryParse(a, out num))
            {
                return "Digite un número";
            }
            else
            {
                int[] numeros = ArreglosRandom(a);
                int menor = numeros.Min();
                int mayor = numeros.Max();
                int suma = 0;
                for (int i = 0; i < numeros.Length; i++)
                {
                    suma += numeros[i];
                }
                int promedio = suma / num;
                return string.Format("El arreglo es: {0}, su mínimo es: {1}, su máximo es: {2} y su promedio es: {3}",
                    ArregloToStr(numeros), menor, mayor, promedio);
            }

        }

    }
}
