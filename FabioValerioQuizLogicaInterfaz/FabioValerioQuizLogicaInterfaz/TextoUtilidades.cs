﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FabioValerioQuizLogicaInterfaz
{
    class TextoUtilidades
    {
        internal string Palindromo(string v)
        {
            string palabra1 = "", pal2 = "";
            string le = "";
            int i = 0, tl = 0;
            palabra1 = v;
            tl = palabra1.Length;
            for (i = tl - 1; i >= 0; i--)
            {
                le = palabra1.Substring(i, 1);
                pal2 = pal2 + le;
            }

            if (palabra1.Equals(pal2))
            {
                return ("Es palíndromo");
            }
            else
            {

                return ("No es palindromo");
            }

        }
        internal string cadena(string a, string b, string c)
        {
            int num1 = 0;
            int num2 = 0;

            if (int.TryParse(b, out num1) && int.TryParse(c, out num2))
            {
                num1 = int.Parse(b);
                num2 = int.Parse(c);
                if (num1 < a.Length && num2 < a.Length && num1 > 0 && num2 > 0)
                {
                    string cadena = a.Substring(num1 - 1, num2);
                    return cadena;
                }
                else
                {
                    return "Esos números son inválidos, la cadena no es tan larga/corta";
                }
            }
            else
            {
                return "Escriba numeros";
            }
        }

        internal string cadenaValida(string a)
        {

            Random rnd = new Random();
            string posibles = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int longitud = posibles.Length;
            char letra;
            int longitudnuevacadena = 5;
            string nuevacadena = "";
            for (int i = 0; i < longitudnuevacadena; i++)
            {
                letra = posibles[rnd.Next(longitud)];
                nuevacadena += letra.ToString();
            }
            char[] abc = nuevacadena.ToArray();
            int cont = 0;
            if (!a.Equals("A") && !a.Equals("a") && !a.Equals("Z") && !a.Equals("9"))
            {
                return "Caracter Incorrecto";
            }
            for (int i = 0; i < abc.Length; i++)
            {
                if (char.IsUpper(abc[i]) && a.Equals("A"))
                {
                    cont++;
                }
                else if (char.IsLower(abc[i]) && a.Equals("a"))
                {
                    cont++;
                }
                else if (char.IsDigit(abc[i]) && a.Equals("9"))
                {
                    cont++;
                }
                else if (char.IsLetterOrDigit(abc[i]) && a.Equals("Z"))
                {
                    cont++;

                }
            }
            if (cont == abc.Length)
            {
                return nuevacadena + " La cadena es válida";
            }
            return nuevacadena + " La cadena no es válida";

        }

        internal string CadenasIguales(string v1, string v2)
        {
            if (v1.Equals(v2))
            {
                return "Son iguales";
            }
            return "No son iguales";
        }
    }
}
