﻿namespace FabioValerioQuizLogicaInterfaz
{
    partial class Menu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnNumeros = new System.Windows.Forms.Button();
            this.btnArreglo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(34, 140);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 60);
            this.button1.TabIndex = 5;
            this.button1.Text = "TextoInterfaz";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnNumeros
            // 
            this.btnNumeros.BackColor = System.Drawing.Color.Black;
            this.btnNumeros.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumeros.ForeColor = System.Drawing.Color.White;
            this.btnNumeros.Location = new System.Drawing.Point(34, 74);
            this.btnNumeros.Name = "btnNumeros";
            this.btnNumeros.Size = new System.Drawing.Size(200, 60);
            this.btnNumeros.TabIndex = 4;
            this.btnNumeros.Text = "NumeroInterfaz";
            this.btnNumeros.UseVisualStyleBackColor = false;
            this.btnNumeros.Click += new System.EventHandler(this.btnNumeros_Click);
            // 
            // btnArreglo
            // 
            this.btnArreglo.BackColor = System.Drawing.Color.Black;
            this.btnArreglo.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnArreglo.ForeColor = System.Drawing.Color.White;
            this.btnArreglo.Location = new System.Drawing.Point(34, 8);
            this.btnArreglo.Name = "btnArreglo";
            this.btnArreglo.Size = new System.Drawing.Size(200, 60);
            this.btnArreglo.TabIndex = 3;
            this.btnArreglo.Text = "ArregloInterfaz";
            this.btnArreglo.UseVisualStyleBackColor = false;
            this.btnArreglo.Click += new System.EventHandler(this.btnArreglo_Click);
            this.btnArreglo.MouseLeave += new System.EventHandler(this.btnArreglo_MouseLeave);
            this.btnArreglo.MouseHover += new System.EventHandler(this.btnArreglo_MouseHover);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(264, 220);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnNumeros);
            this.Controls.Add(this.btnArreglo);
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Principal";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnNumeros;
        private System.Windows.Forms.Button btnArreglo;
    }
}

