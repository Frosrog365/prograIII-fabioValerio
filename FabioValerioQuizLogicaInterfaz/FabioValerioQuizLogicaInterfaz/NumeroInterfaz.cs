﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FabioValerioQuizLogicaInterfaz
{
    public partial class NumeroInterfaz : Form
    {
        private NumeroUtilidades numUti;
        public NumeroInterfaz()
        {
            InitializeComponent();
        }

        private void numeroNarcicistaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = 0;
            if (int.TryParse(textBox1.Text.Trim(), out num)) {
            richTextBox1.Text = numUti.narcicista(num);
        }
            else
            {
                richTextBox1.Text = "Digite un número";
            }
        }

        private void NumeroInterfaz_Load(object sender, EventArgs e)
        {
            numUti = new NumeroUtilidades();
        }

        private void raízAproximadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = numUti.RaizAproximada(textBox1.Text.Trim());

        }

        private void invertirNumeroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = numUti.InvertirNumero(textBox1.Text.Trim());
        }
    }
}
