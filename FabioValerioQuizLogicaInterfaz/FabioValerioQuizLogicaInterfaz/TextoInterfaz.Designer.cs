﻿namespace FabioValerioQuizLogicaInterfaz
{
    partial class TextoInterfaz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ejerciciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.palindromoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.cadenaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadenaVálidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.cadenasIgualesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ejerciciosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ejerciciosToolStripMenuItem
            // 
            this.ejerciciosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.palindromoToolStripMenuItem,
            this.cadenaToolStripMenuItem,
            this.cadenaVálidaToolStripMenuItem,
            this.cadenasIgualesToolStripMenuItem});
            this.ejerciciosToolStripMenuItem.Name = "ejerciciosToolStripMenuItem";
            this.ejerciciosToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.ejerciciosToolStripMenuItem.Text = "Ejercicios";
            // 
            // palindromoToolStripMenuItem
            // 
            this.palindromoToolStripMenuItem.Name = "palindromoToolStripMenuItem";
            this.palindromoToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.palindromoToolStripMenuItem.Text = "Palindromo";
            this.palindromoToolStripMenuItem.Click += new System.EventHandler(this.palindromoToolStripMenuItem_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(13, 77);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(259, 172);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(120, 30);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(36, 20);
            this.textBox2.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(162, 30);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(36, 20);
            this.textBox3.TabIndex = 4;
            // 
            // cadenaToolStripMenuItem
            // 
            this.cadenaToolStripMenuItem.Name = "cadenaToolStripMenuItem";
            this.cadenaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cadenaToolStripMenuItem.Text = "Cadena a y b";
            this.cadenaToolStripMenuItem.Click += new System.EventHandler(this.cadenaToolStripMenuItem_Click);
            // 
            // cadenaVálidaToolStripMenuItem
            // 
            this.cadenaVálidaToolStripMenuItem.Name = "cadenaVálidaToolStripMenuItem";
            this.cadenaVálidaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cadenaVálidaToolStripMenuItem.Text = "Cadena Válida";
            this.cadenaVálidaToolStripMenuItem.Click += new System.EventHandler(this.cadenaVálidaToolStripMenuItem_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(13, 51);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 5;
            this.textBox4.Visible = false;
            // 
            // cadenasIgualesToolStripMenuItem
            // 
            this.cadenasIgualesToolStripMenuItem.Name = "cadenasIgualesToolStripMenuItem";
            this.cadenasIgualesToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.cadenasIgualesToolStripMenuItem.Text = "Cadenas iguales";
            this.cadenasIgualesToolStripMenuItem.Click += new System.EventHandler(this.cadenasIgualesToolStripMenuItem_Click);
            // 
            // TextoInterfaz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TextoInterfaz";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TextoInterfaz";
            this.Load += new System.EventHandler(this.TextoInterfaz_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ejerciciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem palindromoToolStripMenuItem;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ToolStripMenuItem cadenaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadenaVálidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadenasIgualesToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox4;
    }
}