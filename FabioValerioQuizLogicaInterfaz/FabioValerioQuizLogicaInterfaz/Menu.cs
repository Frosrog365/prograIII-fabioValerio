﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FabioValerioQuizLogicaInterfaz
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void btnArreglo_Click(object sender, EventArgs e)
        {
            ArregloInterfaz arr = new ArregloInterfaz();
            arr.ShowDialog();
        }

        private void btnNumeros_Click(object sender, EventArgs e)
        {
            NumeroInterfaz num = new NumeroInterfaz();
            num.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TextoInterfaz txt = new TextoInterfaz();
            txt.ShowDialog();
        }

        private void btnArreglo_MouseHover(object sender, EventArgs e)
        {
            btnArreglo.BackColor = Color.Red;
        }

        private void btnArreglo_MouseLeave(object sender, EventArgs e)
        {
            btnArreglo.BackColor = Color.Black;

        }
    }
}
