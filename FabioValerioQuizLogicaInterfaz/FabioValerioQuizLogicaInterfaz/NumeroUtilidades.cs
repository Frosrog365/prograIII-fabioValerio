﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FabioValerioQuizLogicaInterfaz
{
    class NumeroUtilidades
    {
        internal string narcicista(int v)
        {

            int digitos = 0;
            int n = 0;
            n = v;
            while (n > 0)
            {
                n /= 10;
                digitos++;
            }
            int[] numeros = new int[(int)digitos];
            n = v;
            int cont = 0;
            while (n > 0)
            {
                numeros[cont] = n % 10;
                n /= 10;
                cont++;
            }
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine(numeros[i]);
            }
            double suma = 0;
            string resultado = "";
            for (int i = 0; i < numeros.Length; i++)
            {

                suma += Math.Pow(numeros[i], digitos);
                resultado += string.Format("{0}+", Math.Pow(numeros[i], digitos));
                if (suma == v)
                {
                    return resultado + "\nEs narcicista";
                }
            }
            return "No es narcicista";
        }

        internal string RaizAproximada(string a)
        {
            double num = 0;
            if (double.TryParse(a, out num))
            {
                while (num > 0)
                {
                    if (Math.Sqrt(num) % 1 > 0)
                    {
                        num--;
                    }
                    else
                    {
                        break;
                    }
                }
                return string.Format("La raíz aproximada de {0} es: {1}", a, (Math.Sqrt(num)));
            }
            else
            {
                return "Dato inválido, intente de nuevo";
            }
        }

        internal string InvertirNumero(string v)
        {
            if (!v.Equals(""))
            {
                char[] cadenaNormal = v.ToArray();
                char[] cadenaInvertida = new char[cadenaNormal.Length];
                int cont = 0;
                String cadena = "";
                for (int i = cadenaNormal.Length - 1; i >= 0; i--)
                {
                    cadenaInvertida[cont] = cadenaNormal[i];
                    cadena += cadenaInvertida[cont].ToString();
                }
                double invertido = double.Parse(cadena);
                return String.Format("El número invertido quedaría: {0}", invertido);
            }
            else
            {
                return "El espacio en blanco no debe quedar vacio";
            }
        }
    }
}
