﻿namespace FabioValerioQuizLogicaInterfaz
{
    partial class ArregloInterfaz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.raizAproximadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ejerciciosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordenarAscendentementeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.promedioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rangoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menorMayorYPromedioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ejerciciosToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 67);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(260, 138);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 41);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // raizAproximadaToolStripMenuItem
            // 
            this.raizAproximadaToolStripMenuItem.Name = "raizAproximadaToolStripMenuItem";
            this.raizAproximadaToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.raizAproximadaToolStripMenuItem.Text = "Raiz Aproximada";
            this.raizAproximadaToolStripMenuItem.Click += new System.EventHandler(this.raizAproximadaToolStripMenuItem_Click);
            // 
            // ejerciciosToolStripMenuItem1
            // 
            this.ejerciciosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ordenarAscendentementeToolStripMenuItem1,
            this.promedioToolStripMenuItem1,
            this.rangoToolStripMenuItem,
            this.menorMayorYPromedioToolStripMenuItem});
            this.ejerciciosToolStripMenuItem1.Name = "ejerciciosToolStripMenuItem1";
            this.ejerciciosToolStripMenuItem1.Size = new System.Drawing.Size(68, 20);
            this.ejerciciosToolStripMenuItem1.Text = "Ejercicios";
            this.ejerciciosToolStripMenuItem1.Click += new System.EventHandler(this.ejerciciosToolStripMenuItem1_Click);
            // 
            // ordenarAscendentementeToolStripMenuItem1
            // 
            this.ordenarAscendentementeToolStripMenuItem1.Name = "ordenarAscendentementeToolStripMenuItem1";
            this.ordenarAscendentementeToolStripMenuItem1.Size = new System.Drawing.Size(216, 22);
            this.ordenarAscendentementeToolStripMenuItem1.Text = "Ordenar Ascendentemente";
            this.ordenarAscendentementeToolStripMenuItem1.Click += new System.EventHandler(this.ordenarAscendentementeToolStripMenuItem_Click);
            // 
            // promedioToolStripMenuItem1
            // 
            this.promedioToolStripMenuItem1.Name = "promedioToolStripMenuItem1";
            this.promedioToolStripMenuItem1.Size = new System.Drawing.Size(216, 22);
            this.promedioToolStripMenuItem1.Text = "Promedio";
            this.promedioToolStripMenuItem1.Click += new System.EventHandler(this.promedioToolStripMenuItem_Click);
            // 
            // rangoToolStripMenuItem
            // 
            this.rangoToolStripMenuItem.Name = "rangoToolStripMenuItem";
            this.rangoToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.rangoToolStripMenuItem.Text = "Rango";
            this.rangoToolStripMenuItem.Click += new System.EventHandler(this.rangoToolStripMenuItem_Click);
            // 
            // menorMayorYPromedioToolStripMenuItem
            // 
            this.menorMayorYPromedioToolStripMenuItem.Name = "menorMayorYPromedioToolStripMenuItem";
            this.menorMayorYPromedioToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.menorMayorYPromedioToolStripMenuItem.Text = "Menor, Mayor y promedio";
            this.menorMayorYPromedioToolStripMenuItem.Click += new System.EventHandler(this.menorMayorYPromedioToolStripMenuItem_Click);
            // 
            // ArregloInterfaz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ArregloInterfaz";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ArregloInterfaz";
            this.Load += new System.EventHandler(this.ArregloInterfaz_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem raizAproximadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ejerciciosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ordenarAscendentementeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem promedioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rangoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menorMayorYPromedioToolStripMenuItem;
    }
}