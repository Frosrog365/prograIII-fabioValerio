﻿namespace FabioValerioQuizLogicaInterfaz
{
    partial class NumeroInterfaz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ejerciciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numeroNarcicistaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.raízAproximadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertirNumeroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ejerciciosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ejerciciosToolStripMenuItem
            // 
            this.ejerciciosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.numeroNarcicistaToolStripMenuItem,
            this.raízAproximadaToolStripMenuItem,
            this.invertirNumeroToolStripMenuItem});
            this.ejerciciosToolStripMenuItem.Name = "ejerciciosToolStripMenuItem";
            this.ejerciciosToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.ejerciciosToolStripMenuItem.Text = "Ejercicios";
            // 
            // numeroNarcicistaToolStripMenuItem
            // 
            this.numeroNarcicistaToolStripMenuItem.Name = "numeroNarcicistaToolStripMenuItem";
            this.numeroNarcicistaToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.numeroNarcicistaToolStripMenuItem.Text = "Numero Narcicista";
            this.numeroNarcicistaToolStripMenuItem.Click += new System.EventHandler(this.numeroNarcicistaToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 37);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(13, 64);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(259, 176);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            // 
            // raízAproximadaToolStripMenuItem
            // 
            this.raízAproximadaToolStripMenuItem.Name = "raízAproximadaToolStripMenuItem";
            this.raízAproximadaToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.raízAproximadaToolStripMenuItem.Text = "Raíz Aproximada";
            this.raízAproximadaToolStripMenuItem.Click += new System.EventHandler(this.raízAproximadaToolStripMenuItem_Click);
            // 
            // invertirNumeroToolStripMenuItem
            // 
            this.invertirNumeroToolStripMenuItem.Name = "invertirNumeroToolStripMenuItem";
            this.invertirNumeroToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.invertirNumeroToolStripMenuItem.Text = "Invertir Numero";
            this.invertirNumeroToolStripMenuItem.Click += new System.EventHandler(this.invertirNumeroToolStripMenuItem_Click);
            // 
            // NumeroInterfaz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "NumeroInterfaz";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NumeroInterfaz";
            this.Load += new System.EventHandler(this.NumeroInterfaz_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ejerciciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numeroNarcicistaToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ToolStripMenuItem raízAproximadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertirNumeroToolStripMenuItem;
    }
}