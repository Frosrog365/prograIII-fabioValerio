﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FabioValerioQuizLogicaInterfaz
{
    public partial class TextoInterfaz : Form
    {
        TextoUtilidades txtUti;
        public TextoInterfaz()
        {
            InitializeComponent();
        }

        private void btnArreglo_Click(object sender, EventArgs e)
        {

        }

        private void palindromoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox4.Visible = false;


            if (textBox1.Text.Equals(""))
            {
                richTextBox1.Text = "Digite una palabra";
            }
            else
            {
                richTextBox1.Text = txtUti.Palindromo(textBox1.Text);
            }


        }

        private void TextoInterfaz_Load(object sender, EventArgs e)
        {

            textBox4.Visible = false;

            txtUti = new TextoUtilidades();
        }

        private void cadenaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox4.Visible = false;



            if (textBox1.Text.Equals("") || textBox2.Text.Equals("") || textBox3.Text.Equals(""))
            {
                richTextBox1.Text = "Digite una cadena de caracteres y los 2 digitos";
            }
            else
            {
                richTextBox1.Text = txtUti.cadena(textBox1.Text, textBox2.Text, textBox3.Text);
            }
        }
        private void limpiar()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            richTextBox1.Text = "";
        }

        private void cadenaVálidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox4.Visible = false;
            richTextBox1.Text = txtUti.cadenaValida(textBox1.Text);
        }

        private void cadenasIgualesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox4.Visible = true;
            if (textBox4.Visible && !textBox1.Text.Equals(""))
            {
                richTextBox1.Text = txtUti.CadenasIguales(textBox1.Text.Trim(), textBox4.Text.Trim());
            }
        }
    }
}
