﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FabioValerioQuizLogicaInterfaz
{
    public partial class ArregloInterfaz : Form
    {
        private ArregloUtilidades arrUti;
        public ArregloInterfaz()
        {
            InitializeComponent();

        }
        private void ArregloInterfaz_Load(object sender, EventArgs e)
        {
            arrUti = new ArregloUtilidades();
        }

        private void ordenarAscendentementeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int lim = 0;
            if (int.TryParse(textBox1.Text, out lim))
            {
                richTextBox1.Text = arrUti.OrdenarAscendente(textBox1.Text.Trim());
            }
            else
            {
                richTextBox1.Text = "Escriba la cantidad de números del arreglo";
            }
        }

        private void rangoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = arrUti.rango(textBox1.Text.Trim());
        }

        private void raizAproximadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void promedioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = arrUti.promedio(textBox1.Text.Trim());

        }

        private void randoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ejerciciosToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void menorMayorYPromedioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = arrUti.TodoJunto(textBox1.Text.Trim());
        }
    }
}
