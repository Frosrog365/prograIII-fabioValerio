﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu m = new Menu();
            Console.WriteLine("Digite el primer numero: ");
            m.setNum1(int.Parse(Console.ReadLine()));
            Console.WriteLine("Digite el segundo numero: ");
            m.setNum2(int.Parse(Console.ReadLine()));
            Operaciones o = new Operaciones(m.getNum1(), m.getNum2());
            while (true)
            {
                Console.WriteLine(m.getMenu());

                int opcion = int.Parse(Console.ReadLine());

                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("El resultado es {0}", o.sumar());
                        break;
                    case 2:
                        Console.WriteLine("El resultado es {0}", o.restar());
                        break;
                    case 3:
                        Console.WriteLine("El resultado es {0}", o.multiplicar());
                        break;
                    case 4:
                        Console.WriteLine("El resultado es {0}", o.dividir());
                        break;
                    case 5:

                        o.agregarNumeros();
                        break;
                    case 6:
                        if (o.getArreglo() != null)
                        {
                            foreach (int numero in o.getArreglo())
                            {
                                Console.WriteLine(numero);
                            }
                        }
                        else
                        {
                            Console.WriteLine("El arreglo está vacío");
                        }

                        break;
                    case 7:
                        Environment.Exit(0);
                        break;
                    case 8:
                        Console.WriteLine("Digite su nombre: ");
                        string a = Console.ReadLine();
                        HolaUsuario hU = new HolaUsuario(a);
                        hU.saludar();
                        break;
                    case 9:
                        bool bandera = true;
                        while (bandera)
                        {
                            Console.WriteLine("Digite el primer numero: ");
                            int num1 = int.Parse(Console.ReadLine());
                            Console.WriteLine("Digite el segundo numero: ");
                            int num2 = int.Parse(Console.ReadLine());
                            Console.WriteLine("Digite el tercer numero: ");
                            int num3 = int.Parse(Console.ReadLine());


                            TresNumeros tN = new TresNumeros(num1, num2, num3);
                            Console.WriteLine(tN.sacarNumeros());
                            Console.WriteLine("Desea continuar 1.Si, 2.No?");
                            int d = int.Parse(Console.ReadLine());
                            if (d == 1)
                            {

                            }
                            else
                            {
                                bandera = false;
                            }
                        }
                      
                        break;
                    default:
                        break;

                }
            }

        }

    }
}
