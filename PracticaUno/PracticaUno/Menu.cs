﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Menu
    {
        private string menu;
        private int num1;
        private int num2;
        private int[] arreglo;

        public Menu()
        {
            this.menu = "1. Sumar\n" +
            "2. Restar\n" +
            "3. Multiplicar\n" +
            "4. Dividir\n" +
            "5. Llenar un arreglo\n" +
            "6. Imprimir el arreglo\n" +
            "7. Salir\n" +
            "8. Saludar\n" +
            "9. Tres Numeros";
            this.arreglo = null;

        }
        public string getMenu()
        {
            return menu;
        }
        public int getNum1()
        {
            return num1;
        }
        public int getNum2()
        {
            return num2;
        }
        public void setNum1(int num1)
        {
            this.num1 = num1;
        }
        public void setNum2(int num2)
        {
            this.num2 = num2;

        }
        public int[] getArreglo()
        {
            return arreglo;
        }
        public void setArreglo(int[] arreglo)
        {
            this.arreglo = arreglo;
        }
    }
}
