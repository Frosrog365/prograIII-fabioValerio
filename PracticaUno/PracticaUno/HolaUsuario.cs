﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class HolaUsuario
    {
        private string nombreUsuario;
        private string mydate;

        
        public HolaUsuario(string nombreUsuario)
        {
            this.nombreUsuario = nombreUsuario;
            this.mydate = DateTime.Now.ToString("dd/MM/yyyy");
        }
        public void saludar()
        {
            Console.WriteLine("Hola {0} el día de hoy es {1}", nombreUsuario, mydate);
        }
    }
}
