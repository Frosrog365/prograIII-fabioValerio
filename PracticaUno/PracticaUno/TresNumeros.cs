﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class TresNumeros
    {
        private int numero1;
        private int numero2;
        private int numero3;
        public TresNumeros(int num1, int num2, int num3)
        {
            numero1 = num1;
            numero2 = num2;
            numero3 = num3;
        }
        public int sacarNumeros()
        {
            if (numero1 > 0)
            {
                return numero2 * numero3;
            }
            else
            {
                return numero2 + numero3;
            }
        }
    }
}
