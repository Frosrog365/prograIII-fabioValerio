﻿using System;

public class Operaciones
{
    private int numero1;
    private int numero2;
    private int[] arreglo;
    public Operaciones(int numero1, int numero2)
    {
        this.numero1 = numero1;
        this.numero2 = numero2;
        this.arreglo = null;

    }
    public int getNumero1()
    {
        return numero1;
    }
    public int getNumero2()
    {
        return numero2;
    }
    public int sumar()
    {
        return numero1 + numero2;
    }
    public int restar()
    {
        return numero1 - numero2;
    }
    public int multiplicar()
    {
        return numero1 * numero2;
    }
    public int dividir()
    {
        return numero1 / numero2;
    }
    public int[] getArreglo()
    {
        return this.arreglo;
    }
    public void agregarNumeros()
    {
        Console.WriteLine("Digite el número");
        int num = int.Parse(Console.ReadLine());
        this.arreglo = new int[num];
        for (int i = 0; i < num; i++)
        {
            arreglo[i] = num;
        }
    }
}
