﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DemoWebService.BCCR;

namespace DemoWebService
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dolar d = new Dolar();
            d.dolar = d.sacarDolares();
            d.dolar = d.dolar.Replace('.', ',');
            if (!String.IsNullOrEmpty(txtDolares.Text))
            {
                decimal dec = Convert.ToDecimal(d.dolar) * Convert.ToDecimal(txtDolares.Text);
                richTextBox1.Text = "₡" + dec.ToString("0.##");
            }
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            wsIndicadoresEconomicosSoapClient ws =
               new wsIndicadoresEconomicosSoapClient("wsIndicadoresEconomicosSoap");
            richTextBox1.Text = ws.ObtenerIndicadoresEconomicosXML("318", DateTime.Now.ToString("dd/MM/yyyy"),
                DateTime.Now.ToString("dd/MM/yyyy"), "UTN", "N");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void txtDolares_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dolar d = new Dolar();
            d.dolar = d.sacarDolares();
            d.dolar = d.dolar.Replace('.', ',');
            if (!String.IsNullOrEmpty(textBox1.Text))
            {
                decimal dec = Convert.ToDecimal(textBox1.Text)/Convert.ToDecimal(d.dolar);
                richTextBox1.Text = "$" + dec.ToString("0.##");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
