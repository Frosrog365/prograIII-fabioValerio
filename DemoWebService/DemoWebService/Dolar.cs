﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DemoWebService.BCCR;

namespace DemoWebService
{
    class Dolar
    {
        public String dolar { get; set; }
        wsIndicadoresEconomicosSoapClient ws =
                new wsIndicadoresEconomicosSoapClient("wsIndicadoresEconomicosSoap");

        XmlDocument doc = new XmlDocument();
        public string sacarDolares()
        {
            doc.LoadXml(ws.ObtenerIndicadoresEconomicosXML("318", DateTime.Now.ToString("dd/MM/yyyy"),
                DateTime.Now.ToString("dd/MM/yyyy"), "UTN", "N"));

            XmlNodeList ListaDolar;
            

            ListaDolar = doc.GetElementsByTagName("NUM_VALOR");
            string dolar = ListaDolar[0].InnerText;

            return dolar;
        }


    }
}

