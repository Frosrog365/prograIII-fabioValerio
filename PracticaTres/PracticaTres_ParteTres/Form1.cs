﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PracticaTres_ParteTres
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Negrita_CheckedChanged(object sender, EventArgs e)
        {
            this.label4.Font = new Font(this.label4.Font.FontFamily,
            this.label4.Font.Size, this.label4.Font.Style ^ FontStyle.Bold);
        }

        private void Cursiva_CheckedChanged(object sender, EventArgs e)
        {
            this.label4.Font = new Font(this.label4.Font.FontFamily,
            this.label4.Font.Size, this.label4.Font.Style ^ FontStyle.Italic);
        }

        private void Tachado_CheckedChanged(object sender, EventArgs e)
        {
            this.label4.Font = new Font(this.label4.Font.FontFamily,
            this.label4.Font.Size, this.label4.Font.Style ^ FontStyle.Strikeout);
        }

        private void Subrayado_CheckedChanged(object sender, EventArgs e)
        {
            this.label4.Font = new Font(this.label4.Font.FontFamily,
            this.label4.Font.Size, this.label4.Font.Style ^ FontStyle.Underline);
        }

        private void Consolas_CheckedChanged(object sender, EventArgs e)
        {
            FontFamily csl = new FontFamily("Consolas");
            this.label4.Font = new Font(csl, this.label4.Font.Size, this.label4.Font.Style);
        }

        private void Colonna_ChekedChanged(object sender, EventArgs e)
        {
            FontFamily csl = new FontFamily("Colonna MT");
            this.label4.Font = new Font(csl, this.label4.Font.Size, this.label4.Font.Style);
        }

        private void Verdana_CheckedChanged(object sender, EventArgs e)
        {
            FontFamily csl = new FontFamily("Verdana");
            this.label4.Font = new Font(csl, this.label4.Font.Size, this.label4.Font.Style);
        }

        private void Broadway_CheckedChanged(object sender, EventArgs e)
        {

            FontFamily csl = new FontFamily("Broadway");
            this.label4.Font = new Font(csl, this.label4.Font.Size, this.label4.Font.Style);
        }

        private void ocho_CheckedChanged(object sender, EventArgs e)
        {
            this.label4.Font = new Font(this.label4.Font.FontFamily, 8, this.label4.Font.Style);
        }

        private void doce_CheckedChanged(object sender, EventArgs e)
        {
            this.label4.Font = new Font(this.label4.Font.FontFamily, 12, this.label4.Font.Style);

        }

        private void dieciseis_CheckedChanged(object sender, EventArgs e)
        {
            this.label4.Font = new Font(this.label4.Font.FontFamily, 16, this.label4.Font.Style);

        }

        private void veinte_CheckedChanged(object sender, EventArgs e)
        {
            this.label4.Font = new Font(this.label4.Font.FontFamily, 20, this.label4.Font.Style);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FontFamily mss = new FontFamily("Microsoft Sans Serif");
            this.label4.Font = new Font(mss, 12, FontStyle.Regular);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
