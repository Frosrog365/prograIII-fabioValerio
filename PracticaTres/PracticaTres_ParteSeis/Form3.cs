﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PracticaTres_ParteSeis
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        public string Nombre
        {
            set
            {
                label1.Text = value;
            }
        }

        public string Calle
        {
            get
            {
                return textBox1.Text;
            }
        }
        public string Colonia
        {
            get
            {
                return textBox2.Text;
            }
        }
        public string Delegacion
        {
            get
            {
                return textBox3.Text;
            }
        }
        public string CodigoPostal
        {
            get
            {
                return textBox4.Text;
            }
        }
        public string telefono
        {
            get
            {
                return textBox5.Text;

            }
        }


        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
