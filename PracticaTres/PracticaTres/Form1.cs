﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PracticaTres
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnMultiplicar_Click(object sender, EventArgs e)
        {
            double ct1, ct2, m;
            string r;

            ct1 = Convert.ToDouble(txtOperando1.Text);
            ct2 = Convert.ToDouble(txtOperando2.Text);
            m = ct1 * ct2;
            r = (String.Format("{0:F2}", m));
            txtResultado.Text = r;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtOperando1.Text = "";
            txtOperando2.Text = "";
            txtResultado.Text = "";


        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
    }
