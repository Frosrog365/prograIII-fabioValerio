﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisorImagenesEN;

namespace VisorImagenesBOL
{
    public class BOL
    {
        public List<Foto> CargarImagenes()
        {
            VisorImagenesDAL.DAL dal = new VisorImagenesDAL.DAL();
           
                return dal.CargarImagenes();
            
        }

        public void InsertarImagen(Foto nueva)
        {
            VisorImagenesDAL.DAL dal = new VisorImagenesDAL.DAL();

            dal.InsertarImagenes(nueva);
        }
    }
}
