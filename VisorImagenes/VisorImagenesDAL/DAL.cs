﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisorImagenesEN;

namespace VisorImagenesDAL
{
    public class DAL
    {
        public List<Foto> CargarImagenes()
        {

            List<Foto> fotos = new List<Foto>();
            Foto imagen = new Foto();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"SELECT foto,Id from foto;"
;
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                byte[] fotoa = new byte[0];

                while (reader.Read())
                {
                    fotos.Add(cargarFoto(reader));
                }
                return fotos;
            }
        }

        public void InsertarImagenes(Foto nueva)
        {
            {
                using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
                {

                    con.Open();
                    string sql = @"INSERT INTO foto(foto) VALUES (@foto) returning id";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                    MemoryStream stream = new MemoryStream();
                    nueva.Imagen.Save(stream, ImageFormat.Jpeg); //Foto es de tipo Image en C#
                    byte[] pic = stream.ToArray();
                    cmd.Parameters.AddWithValue("@foto", pic);
                    nueva.Id = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
        }

        private Foto cargarFoto(NpgsqlDataReader reader)
        {
            Foto imagen = new Foto
            {

                Id = reader["Id"] != DBNull.Value ? Convert.ToInt32(reader["Id"]) : 0,

            };
            byte[] foto = new byte[0];

            foto = (byte[])reader["foto"];
            MemoryStream stream = new MemoryStream(foto);
            imagen.Imagen = Image.FromStream(stream); //f.Foto es tipo Image en c#

            return imagen;
        }
    }
}
