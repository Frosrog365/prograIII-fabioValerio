﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCapasDAL;
using DemoCapasENL;

namespace DemoCapasBOL
{
    public class TestBOL
    {
        public string Prueba()
        {
            //Validaciones, procesar información 
            TestDAL dal = new TestDAL();
            return dal.PruebaDAL();
        }

        public List<Eusuario> cargarObjetos()
        {
            TestDAL dal = new TestDAL();
            return dal.cargarUsuarios();
        }
    }
}
