﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCapasENL;
using Npgsql;

namespace DemoCapasDAL
{
    public class TestDAL
    {
        public string PruebaDAL()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();

                string usuarios = "";
                string sql = "select id, cedula, usuario from usuario;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    usuarios += reader.GetInt32(0) + "\t";
                    usuarios += reader.GetString(1) + "\t";
                    usuarios += reader.GetString(2) + "\n";
                }
                return usuarios;
            }


        }

        public List<Eusuario> cargarUsuarios()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                List<Eusuario> usuarios = new List<Eusuario>();
                con.Open();

                
                string sql = "select id, cedula, usuario, nombre, apellido_uno, apellido_dos, pass, email from usuario;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Eusuario nuevo = new Eusuario();
                    nuevo.id = reader.GetInt32(0);
                    nuevo.cedula = reader.GetString(1);
                    nuevo.usuario = reader.GetString(2);
                    nuevo.nombre = reader.GetString(3);
                    nuevo.primer_apellido = reader.GetString(4);
                    nuevo.segudno_apellido = reader.GetString(5);
                    nuevo.pass = reader.GetString(6);
                    nuevo.email = reader.GetString(7);
                    usuarios.Add(nuevo);
                    
                    
                }
                return usuarios;

            }
        }
    }
}
