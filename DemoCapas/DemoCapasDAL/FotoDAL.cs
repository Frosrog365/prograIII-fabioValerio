﻿using DemoCapasENL;
using Npgsql;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace DemoCapasDAL
{
    public class FotoDAL
    {
        public EFoto Insertar(EFoto foto)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();
                string sql = @"INSERT INTO foto(foto) VALUES (@foto) returning id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                foto.Imagen.Save(stream, ImageFormat.Jpeg); //Foto es de tipo Image en C#
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@foto", pic);
                foto.Id = Convert.ToInt32(cmd.ExecuteScalar());
                return foto;
            }
        }

        public EFoto modificar(EFoto foto)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();
                string sql = @"UPDATE foto set foto = @foto";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                foto.Imagen.Save(stream, ImageFormat.Jpeg); //Foto es de tipo Image en C#
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@foto", pic);
                return foto;
            }
        }

        public EFoto CargarPorID(int FotoId)
        {
            EFoto foto = new EFoto();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"SELECT foto from foto where id = @id";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", FotoId);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                byte[] fotoa = new byte[0];
                if (reader.Read())
                {
                    fotoa = (byte[])reader["foto"];
                    MemoryStream stream = new MemoryStream(fotoa);
                    foto.Imagen = Image.FromStream(stream); //f.Foto es tipo Image en c#
                    foto.Id = FotoId;
                }
                return foto;
            }
        }

        public void eliminar(EFoto fotoAux)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"DELETE FROM foto WHERE id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                Console.WriteLine(fotoAux.Id);
                cmd.Parameters.AddWithValue("@id", fotoAux.Id);
                cmd.ExecuteNonQuery();
            }
        }
    }
}