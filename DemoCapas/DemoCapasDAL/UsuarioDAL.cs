﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCapasENL;
using Npgsql;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace DemoCapasDAL
{
    public class UsuarioDAL
    {
       
        public List<EFoto> CargarFotos(int num)
        {
            List<EFoto> fotos = new List<EFoto>();

            return fotos;
        }
        public List<EUsuario> CargarTodo(string filtro)
        {
            List<EUsuario> usuarios = new List<EUsuario>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id, cedula, usuario, 
	                            nombre, apellido_uno, apellido_dos, 
		                            pass, email,id_foto from usuario where
		                            lower(cedula) like @filtro
		                            or lower(nombre) like  @filtro
		                            or lower(usuario) like  @filtro
		                            or lower(apellido_uno) like  @filtro
		                            or lower(email) like  @filtro";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@filtro", filtro.ToLower() + "%");
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    usuarios.Add(CargarUsuario(reader));
                }

            }

            return usuarios;
        }

        public EUsuario Verificar(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id, cedula, usuario, 
                               nombre, apellido_uno, apellido_dos, 
                                pass, email, id_foto from usuario where (usuario = @usuario or email = @email)
                                and pass = @pass";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usuario", usuario.Usuario);
                cmd.Parameters.AddWithValue("@email", usuario.Email);
                cmd.Parameters.AddWithValue("@pass", usuario.Password);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarUsuario(reader);
                }
            }
            return null;
        }


        public bool Modificar(EUsuario usuario)
        {



            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();
                string sql = @"UPDATE usuario
	                            SET cedula=@ced, usuario=@usu, nombre=@nom, 
	                                apellido_uno=@apu, apellido_dos=@aps, 
		                            pass=@pas, email=@ema, id_foto=@foto
	                            WHERE id=@id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", usuario.Cedula);
                cmd.Parameters.AddWithValue("@usu", usuario.Usuario);
                cmd.Parameters.AddWithValue("@nom", usuario.Nombre);
                cmd.Parameters.AddWithValue("@apu", usuario.ApellidoUno);
                cmd.Parameters.AddWithValue("@aps", usuario.ApellidoDos);
                cmd.Parameters.AddWithValue("@pas", usuario.Password);
                cmd.Parameters.AddWithValue("@ema", usuario.Email);
                cmd.Parameters.AddWithValue("@id", usuario.Id);
                if (usuario.Foto.Id!=0)
                {
                    FotoDAL fdal = new FotoDAL();

                    EFoto FotoAux = usuario.Foto;
                    usuario.Foto = fdal.modificar(usuario.Foto);
                    cmd.Parameters.AddWithValue("@foto", usuario.Foto.Id);
                }
                else if(usuario.Foto.Imagen!=null || usuario.Foto.Id == 0)
                {
                    FotoDAL fdal = new FotoDAL();
                    usuario.Foto = fdal.Insertar(usuario.Foto);
                    cmd.Parameters.AddWithValue("@foto", usuario.Foto.Id);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@foto", DBNull.Value);
                }
                return cmd.ExecuteNonQuery() > 0;
            }
        }


        public void Eliminar(int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"DELETE FROM usuario WHERE id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
        }

        public bool Insertar(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"INSERT INTO usuario(cedula, usuario, nombre,
                                apellido_uno, apellido_dos, pass, email,id_foto)
	                            VALUES (@ced, @usu, @nom, @apu, 
	                            @aps, @pas, @ema,@foto);";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", usuario.Cedula);
                cmd.Parameters.AddWithValue("@usu", usuario.Usuario);
                cmd.Parameters.AddWithValue("@nom", usuario.Nombre);
                cmd.Parameters.AddWithValue("@apu", usuario.ApellidoUno);
                cmd.Parameters.AddWithValue("@aps", usuario.ApellidoDos);
                cmd.Parameters.AddWithValue("@pas", usuario.Password);
                cmd.Parameters.AddWithValue("@ema", usuario.Email);
                

                
                if(usuario.Foto != null)
                {
                    FotoDAL fdal = new FotoDAL();
                    usuario.Foto = fdal.Insertar(usuario.Foto);
                    cmd.Parameters.AddWithValue("@foto", usuario.Foto.Id);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@foto", DBNull.Value);
                }

                return cmd.ExecuteNonQuery() > 0;
            }
        }
        

        

        private EUsuario CargarUsuario(NpgsqlDataReader reader)
        {
            EUsuario usu = new EUsuario
            {
                //No validar la llave primaria, no es necesario, solo de ejemplo xD
                Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0,
                Cedula = reader["cedula"] != DBNull.Value ? reader["cedula"].ToString() : "Indocumentado",
                Usuario = reader["usuario"].ToString(),
                Nombre = reader["nombre"].ToString(),
                ApellidoUno = reader["apellido_uno"].ToString(),
                ApellidoDos = reader["apellido_dos"].ToString(),
                Password = reader["pass"].ToString(),
                Email = reader["email"].ToString(),
        };
            
            if (reader["id_foto"].ToString().Equals("-1"))
            {
                usu.Foto.Imagen = null;
            }
            else
            {
                usu.Foto = new EFoto();
                usu.Foto.Id = reader["id_foto"] != DBNull.Value ? Convert.ToInt32(reader["id_foto"]) : 0;
                Console.WriteLine(usu.Foto.Id);
                FotoDAL fdal = new FotoDAL();
                usu.Foto = fdal.CargarPorID(usu.Foto.Id);
            }
            return usu;
        }
    }
}
